import sys
import htmlParser
import yaml
import os

path = os.path.dirname(os.path.realpath(__file__))

with open(path + "\\urls.yaml", 'r') as stream:
    try:
        config = yaml.load(stream)

        try:
            restaurant = str(sys.argv[1])
            url = config[restaurant]
            present_menu = getattr(htmlParser, restaurant.strip()) # if restaurant is porten, call function porten from htmlParser
            present_menu(url) # pass the url to the function
            
        except:
            # If there's no argument, just take the first URL
            print("No match for your query")
            print("Presenting you Nya etage by default")
            url = config['etage']
            htmlParser.etage(url)

    except yaml.YAMLError as e:
        print(e)